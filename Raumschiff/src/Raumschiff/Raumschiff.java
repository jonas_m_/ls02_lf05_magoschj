package Raumschiff;

import java.util.ArrayList;

public class Raumschiff {

	private String schiffsname;
	private int photonentorpedoAnzahl;
	private int energieversorgungInProzent;
	private int schildeInProzent;
	private int huelleInProzent;
	private int lebenserhaltungssystemeInProzent;
	private int androidenAnzahl;

	ArrayList<String> broadcastKommunikator = new ArrayList<String>();
	ArrayList<Ladung> ladungsverzeichnis = new ArrayList<Ladung>();

	// Raumschiff
	public Raumschiff() {

	}

	public Raumschiff(String schiffsname, int photonentorpedoAnzahl, int energieversorgungInProzent,
			int schildeInProzent, int huelleInProzent, int lebenserhaltungssystemeInProzent, int androidenAnzahl) {

		this.schiffsname = schiffsname;
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
		this.energieversorgungInProzent = energieversorgungInProzent;
		this.schildeInProzent = schildeInProzent;
		this.huelleInProzent = huelleInProzent;
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
		this.androidenAnzahl = androidenAnzahl;

	}

	// Raumschiff Zustand

	public void ausgabeZustand() {
		System.out.println("\n" + schiffsname + "'s " + "Status: \n");
		System.out.println("Anzahl der Photonentorpedos: " + getPhotonentorpedo());
		System.out.println("Status der Energieversorgung: " + getEnergieversorgung() + "%");
		System.out.println("Status der Schilde: " + getSchilde() + "%");
		System.out.println("Status der H�lle: " + getHuelle() + "%");
		System.out.println("Status der Lebenserhaltungssysteme: " + getLebenserhaltungssysteme() + "%");
		System.out.println("Anzahl der Androiden: " + getAndroiden());

	}

	// Schiffsname
	public void setSchiffsname(String schiffsname) {
		this.schiffsname = schiffsname;
	}

	public String getSchiffsname() {
		return schiffsname;
	}

	// Photonentorpedo
	public void setPhotonentorpedo(int photonentorpedoAnzahl) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
	}

	public int getPhotonentorpedo() {
		return photonentorpedoAnzahl;
	}

	// Energieversorgung
	public void setEnergieversorgung(int energieversorgungInProzent) {
		this.energieversorgungInProzent = energieversorgungInProzent;
	}

	public int getEnergieversorgung() {
		return energieversorgungInProzent;
	}

	// Schilde
	public void setSchilde(int schildeInProzent) {
		this.schildeInProzent = schildeInProzent;
	}

	public int getSchilde() {
		return schildeInProzent;
	}

	// Huelle
	public void setHuelle(int huelleInProzent) {
		this.huelleInProzent = huelleInProzent;
	}

	public int getHuelle() {
		return huelleInProzent;
	}

	// Lebenserhaltungssysteme
	public void setLebenserhaltungssysteme(int lebenserhaltungssystemeInProzent) {
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
	}

	public int getLebenserhaltungssysteme() {
		return lebenserhaltungssystemeInProzent;
	}

	// Androiden
	public void setAndroiden(int androidenAnzahl) {
		this.androidenAnzahl = androidenAnzahl;
	}

	public int getAndroiden() {
		return androidenAnzahl;
	}

	//

	// Photonentorpedo schiessen
	public void photonentorpedoSchiessen(String schiffsname) {

		if (photonentorpedoAnzahl == 0) {
			nachrichtAnAlle("-=*Click*=-");

		} else {
			photonentorpedoAnzahl -= 1;
			nachrichtAnAlle("Photonentorpedo abgeschossen");
			nachrichtAnAlle(schiffsname + " wurde getroffen!");
//			treffer(schiffsname);
		}

	}

	// Phaserkanone schiessen
	public void phaserkanoneSchiessen(String schiffsname) {

		if (energieversorgungInProzent < 50) {
			nachrichtAnAlle("-=*Click*=-");
		} else {
			energieversorgungInProzent -= 50;
			nachrichtAnAlle("Phaserkanone abgeschossen");
			nachrichtAnAlle(schiffsname + " wurde getroffen!");
//			treffer(schiffsname);
		}

	}

//	// Treffer
//	private void treffer() {
//
//		setSchilde(getSchilde() - 50);
//	}

	// Nachricht an alle
	public void nachrichtAnAlle(String nachricht) {
		System.out.println("\n[" + schiffsname + "]: " + nachricht);
		broadcastKommunikator.add(nachricht);
	}

	// Broadcastkommunikator ausgeben
	public void broadcastKommunikatorAusgeben() {

		System.out.println("\nBrodcast Kommunikator:");

		for (int i = 0; i < broadcastKommunikator.size(); i++)

			System.out.println(broadcastKommunikator.get(i));
	}

	// Ladung hinzufuegen

	public void addLadung(Ladung ladung) {

		this.ladungsverzeichnis.add(ladung);

	}
	// Eintr�ge Logbuch zurueckgeben

	// Photonentorpedos laden

	// Reparatur durchfuehren

	// Raumschiff Zustand

	// Ladungsverzeichnis ausgeben
	public void ausgabeLadungsverzeichnis() {

		System.out.println("\nLadung des Raumschiffes:\n");

		for (int i = 0; i < ladungsverzeichnis.size(); i++) {
			System.out
					.println(ladungsverzeichnis.get(i).getBezeichnung() + " = " + ladungsverzeichnis.get(i).getMenge());
		}
	}

	// Ladungsverzeichnis aufraeumen

}