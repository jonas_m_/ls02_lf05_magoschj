package Raumschiff;


public class AbfrageRaumschiff {
	public static void main(String[] args) {

		// Neues Raumschiff & neue Ladung erstellen
		Raumschiff klingonen = new Raumschiff("IKS Hegh'ta", 1, 100, 100, 100, 100, 2);
		Raumschiff romulaner = new Raumschiff("IRW Khazara", 2, 100, 100, 100, 100, 2);
		Raumschiff vulkanier = new Raumschiff("Ni'Var", 0, 80, 80, 50, 100, 5);

		klingonen.addLadung(new Ladung("Ferengi Schneckensaft", 200));
		klingonen.addLadung(new Ladung("Bat'leth Klingonen Schwert", 200));
		romulaner.addLadung(new Ladung("Borg-Schrott", 5));
		romulaner.addLadung(new Ladung("Rote Materie", 2));
		romulaner.addLadung(new Ladung("Plasma-Waffe", 50));
		vulkanier.addLadung(new Ladung("Forschungssonde", 35));
		vulkanier.addLadung(new Ladung("Photonentorpedo", 3));

		// Klingonen schie�en Photonentorpedo ein Mal auf die Romulaner

		klingonen.photonentorpedoSchiessen(romulaner.getSchiffsname());
		romulaner.setSchilde(romulaner.getSchilde() - 50);
		System.out.println("\n------------------------------");

		// Romulaner schie�en mit Phaserkanone zur�ck

		romulaner.phaserkanoneSchiessen(klingonen.getSchiffsname());
		klingonen.setSchilde(klingonen.getSchilde() - 50);
		vulkanier.nachrichtAnAlle("'Gewalt ist nicht logisch'");
		System.out.println("\n------------------------------");

		// Klingonen rufen Zustand und Ladungsverzeichnis ab

		klingonen.ausgabeZustand();
		klingonen.ausgabeLadungsverzeichnis();
		System.out.println("\n------------------------------");

		// Klingonen schie�en mit zwei weiteren Photonentorpedo auf die Romulaner

		klingonen.photonentorpedoSchiessen(romulaner.getSchiffsname());
		klingonen.photonentorpedoSchiessen(romulaner.getSchiffsname());
		System.out.println("\n------------------------------");

//		Klingonen, Romulaner, Vulkanier rufen Zustand und Ladungsverzeichnis ab

		klingonen.ausgabeZustand();
		klingonen.ausgabeLadungsverzeichnis();
		System.out.println("\n------------------------------");

		romulaner.ausgabeZustand();
		romulaner.ausgabeLadungsverzeichnis();
		System.out.println("\n------------------------------");

		vulkanier.ausgabeZustand();
		vulkanier.ausgabeLadungsverzeichnis();
		System.out.println("\n------------------------------");
	}
}
