package Raumschiff;

public class Ladung {

	private String bezeichnung;
	private int menge;

	// Ladung
	public Ladung() {
	}

	public Ladung(String bezeichnung, int menge) {
		setBezeichnung(bezeichnung);
		setMenge(menge);
	}

	// Bezeichnung
	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}

	public String getBezeichnung() {
		return bezeichnung;
	}

	// Menge
	public void setMenge(int menge) {
		this.menge = menge;
	}

	public int getMenge() {
		return menge;
	}

}
